//
//  ViewController.swift
//  Project7
//
//  Created by Роман Хоменко on 02.04.2022.
//

import UIKit

class ViewController: UITableViewController {
    var petitions: [Petition] = []
    var filteredPetitions: [Petition] = []
    var filteredWords: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Petitions"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(showUrlInfo))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(filteringPetitions))
        
        let urlString: String
        
        if navigationController?.tabBarItem.tag == 0 {
            urlString = "https://www.hackingwithswift.com/samples/petitions-1.json"
        } else {
            urlString = "https://www.hackingwithswift.com/samples/petitions-2.json"
        }
        
        if let url = URL(string: urlString) {
            if let data = try? Data(contentsOf: url) {
                parse(json: data)
                return
            }
        }
        
        showError()
    }
    
    func showError() {
        let alert = UIAlertController(title: "Loading error", message: "There was a problem loading the feed; please check your connection and try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
    
    func parse(json: Data) {
        let decoder = JSONDecoder()
        
        if let jsonPetitions = try? decoder.decode(Petitions.self, from: json) {
            petitions = jsonPetitions.results
            filteredPetitions = petitions
            tableView.reloadData()
        }
    }
}

extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPetitions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let petiton = filteredPetitions[indexPath.row]
        
        cell.textLabel?.text = petiton.title
        cell.detailTextLabel?.text = petiton.body
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailViewController()
        detailVC.detailItem = filteredPetitions[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension ViewController {
    @objc func showUrlInfo() {
        let alert = UIAlertController(title: "This data comes from the We The People API of the Whitehouse", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
    
    @objc func filteringPetitions() {
        let alert = UIAlertController(title: "Write word that you want to see in Petitions", message: nil, preferredStyle: .alert)
        alert.addTextField()
        
        alert.addAction(UIAlertAction(title: "Search", style: .default, handler: { [weak self, weak alert] _ in
            self?.filteredPetitions = []
            
            guard let filterWord = alert?.textFields?[0].text else { return }
            let list = filterWord.components(separatedBy: " ")
            
            self?.filteredWords.insert(contentsOf: list, at: 0)
            
            DispatchQueue.global(qos: .userInitiated).async {
                self?.filter()
            }
            }))
            
            alert.addAction(UIAlertAction(title: "Delete last", style: .default, handler: { [weak self] _ in
                
                if self?.filteredWords.count != 0 {
                    self?.filteredWords.removeLast()
                    self?.filteredPetitions = self!.petitions
                } else {
                    return
                }
                
                DispatchQueue.global(qos: .userInitiated).async {
                    self?.filter()
                }
            }))
            
            present(alert, animated: true)
    }
    
    func filter() {
        for petition in petitions {
            for word in filteredWords {
                if petition.title.contains(word) || petition.body.contains(word) {
                    filteredPetitions.append(petition)
                }
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
