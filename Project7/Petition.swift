//
//  Petition.swift
//  Project7
//
//  Created by Роман Хоменко on 02.04.2022.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
